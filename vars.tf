variable "name" {
   description = "Defines the instance name"
   default = "Lab_grupo03"
}
variable "region" {
  description = "Defines the aws region"
  default = "us-east-1"
 }
variable "instance_type" {
  description = "instance type"
  default = "t2.micro"
 }
 variable "subnet_id" {
  description = "Defines the subnet id"
  default = "subnet-065f678fc11f9f17d"
 }
 variable "vpc_security_group_ids" {
  description= "Defines the security group"
  default = ["sg-0dcb49ec4a46095b1"]
 }
 variable "iam_instance_profile" {
    description = "Defines the IAM"
    default = "SSM-Dev-Trilha"
   
 }
 variable "ami" {
    descripdescription = "Defines the AMI"
    default = "ami-08c40ec9ead489470"
   
 }